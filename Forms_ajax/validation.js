// Проверка на валидность  
$(document).ready(function () {

    $("#registerform, #loginform").validate({
        rules: {
            full_name: {
                required: true,
                minlength: 8,
                maxlength: 32,
            },
            email: {
                required: true,
                email: true,
            },
            username: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 16,
            },
        },
        messages: {
            full_name: {
                required: "Это поле обязательно для заполнения",
                minlength: "Полное имя должно быть минимум 8 символов",
                maxlength: "Максимальное число символо - 32",
            },
            email: {
                required: "Это поле обязательно для заполнения",
                email: "Введите пожалуйста корректный e-mail"
            },
            username: {
                required: "Это поле обязательно для заполнения",
                minlength: "Имя должно быть минимум 4 символа",
                maxlength: "Максимальное число символо - 16",
            },
            password: {
                required: "Это поле обязательно для заполнения",
                minlength: "Пароль должен быть минимум 6 символа",
                maxlength: "Пароль должен быть максимум 16 символов",
            },
        }

    });

}); 

