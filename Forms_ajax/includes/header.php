
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Registration and Login</title>
        <link href="css/style.css" media="screen" rel="stylesheet">
        <link href="css/style_menu.css" media="screen" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
        <script src="validation.js"></script>
    </head> 
    <body>
        
  <?php include 'includes/menu.php'?>